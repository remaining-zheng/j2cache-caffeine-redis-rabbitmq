package com.itbac.j2cache.aop.annotatin;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description 缓存注解，模仿Spring CachePut
 * 缓存写入
 *
 * @author itbac
 * @email 1218585258@qq.com
 * @date 2020/6/17
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface J2CachePut {
    String cacheName() default ""; //缓存名称

    String cacheKey(); //缓存key

    int expire() default 3600; //缓存有效期（单位：秒），默认1小时。

    //value 缓存数据类型，不是String 的，需要转换成String
    Class clazz() default String.class;
}

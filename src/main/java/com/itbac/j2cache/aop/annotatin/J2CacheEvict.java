package com.itbac.j2cache.aop.annotatin;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description 缓存注解，模仿Spring CacheEvict
 * 缓存清除
 *
 * @author itbac
 * @email 1218585258@qq.com
 * @date 2020/6/17
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface J2CacheEvict {

    String cacheName() default ""; //缓存名称

    String cacheKey(); //缓存key

    boolean allEntries() default false; // 是否清空所有cacheName的全部数据

}

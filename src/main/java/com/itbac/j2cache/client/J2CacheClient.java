package com.itbac.j2cache.client;

import java.util.List;

/**
 * @description 二级缓存接口
 *
 * @author itbac
 * @email 1218585258@qq.com
 * @date 2020/6/17
 */
public interface J2CacheClient {

    /**
     * 查询
     * @param key
     * @return
     */
    String getValue(String key);

    /**
     * 删除本地缓存 + 删除 Redis 缓存
     * @param key
     * @param notice 是否发MQ通知
     */
    void delKey(String key, boolean notice);

    /**
     * 删除本地缓存
     * @param key
     * @param notice 是否发MQ通知
     */
    void delLocalKey(String key, boolean notice);

    /**
     * 删除本地缓存
     * @param keys
     * @param notice 是否发MQ通知
     */
    void delLocalKeys(List<String> keys, boolean notice);

    /**
     * 删除所有本地缓存
     * @param notice
     */
    void delLocalAll(boolean notice);

    /**
     * 缓存数据到Redis
     * @param key
     * @param value
     */
    void set(String key, String value);

    /**
     * 缓存数据到Redis
     * @param key
     * @param value
     * @param expire 过期时间，单位：秒
     */
    void set(String key, String value, int expire);




}

package com.itbac.j2cache.rabbitmq.constants;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2021/3/7 12:49
 */
@Component
public class RedisQueueName implements LocalCacheNoticeQueueName {

    @Resource
    private RedisTemplate redisTemplate;

    private String name;


    @Override
    public String getName() {
        return name;
    }

    @PostConstruct
    public void  setName(){
        //通过Redis控制数字自增，设置当前节点使用的queue名称
        name = QueueName.QUEUE_NAME_PREFIX +
                redisTemplate.opsForValue().increment(QueueName.QUEUE_NAME_PREFIX);
    }
}
